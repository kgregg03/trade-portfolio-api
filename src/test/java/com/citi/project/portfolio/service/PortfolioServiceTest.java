package com.citi.project.portfolio.service;

import com.citi.project.portfolio.model.Currency;
import com.citi.project.portfolio.model.Portfolio;
import com.citi.project.portfolio.model.PortfolioTradeState;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PortfolioServiceTest {

    private static final Logger LOG = LoggerFactory.getLogger(PortfolioServiceTest.class);

    @Autowired
    private PortfolioService portfolioService;

    @Test
    public void test_save_sanityCheck() {

        LOG.debug("This is a message");

        Portfolio testPortfolio = new Portfolio();

        testPortfolio.setStockTicker("AAPL");
        testPortfolio.setStockQuantity(500);
        testPortfolio.setTradeState(PortfolioTradeState.BUY);
        testPortfolio.setCurrency(Currency.EUR);
        testPortfolio.setAmount(1500.00);
        

        portfolioService.save(testPortfolio);
        
    }

    @Test
    public void test_findAll_sanityCheck() {
        assert(portfolioService.findAll().size() > 0);
    }
    
}
