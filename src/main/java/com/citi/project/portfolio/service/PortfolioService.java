package com.citi.project.portfolio.service;

import java.util.List;

import com.citi.project.portfolio.dao.PortfolioMongoRepo;
import com.citi.project.portfolio.model.Portfolio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Class with methods for sorting information in mongo from the Portfolio class
 * 
 * @see Portfolio
 */
@Component
public class PortfolioService {
    
    @Autowired
    private PortfolioMongoRepo mongoRepo;

    public List<Portfolio> findAll() {
        return mongoRepo.findAll();

    }

    public Portfolio save(Portfolio portfolio) {
        return mongoRepo.save(portfolio);
    }

    public Portfolio update(String id, Portfolio portfolio){
        portfolio.setId(id);

        return mongoRepo.save(portfolio);
    }
}
