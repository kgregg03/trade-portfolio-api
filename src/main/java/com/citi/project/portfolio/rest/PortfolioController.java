package com.citi.project.portfolio.rest;

import java.util.List;

import com.citi.project.portfolio.model.Portfolio;
import com.citi.project.portfolio.service.PortfolioService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**

 */
@RestController
@RequestMapping("/v1/portfolio")
public class PortfolioController {

    private static final Logger LOG = LoggerFactory.getLogger(PortfolioController.class);

    @Autowired
    private PortfolioService portfolioService;

    @RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<Portfolio> save(@RequestBody Portfolio portfolio) {
        LOG.debug("save portfolio request received");

        return new ResponseEntity<Portfolio>(portfolioService.save(portfolio),
                                            HttpStatus.CREATED);
    }

    @RequestMapping(method=RequestMethod.GET)
    public List<Portfolio> findAll() {
        LOG.debug("findAll() portfolio request received");
        return portfolioService.findAll();
    }

    @RequestMapping(path="/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Portfolio> update(@PathVariable String id,
                                           @RequestBody Portfolio portfolio) {
        LOG.debug("update() request received : " + id);

        return new ResponseEntity<Portfolio>(portfolioService.update(id, portfolio),
                                            HttpStatus.OK);
    }

}