package com.citi.project.portfolio.model;

/**
 * This class sets and updates your portfolio with what stocks and currency you own
 * 
 * 
 */
public class Portfolio {

    private String id;
    private String stockTicker;
    private int stockQuantity;
    private PortfolioTradeState tradeState;
    private Currency currency;
    private double amount;

    // constructors

    public Portfolio() {

    } 

    public Portfolio(String id, String stockTicker, int stockQuantity, PortfolioTradeState tradeState) {
        this.id = id;
        this.stockTicker = stockTicker;
        this.stockQuantity = stockQuantity;
        this.tradeState = tradeState;
    }

    public Portfolio(String id, Currency currency, double amount) {
        this.id = id;
        this.currency = currency;
        this.amount = amount;
    }

    // getters and setters

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public PortfolioTradeState getTradeState() {
        return tradeState;
    }

    public void setTradeState(PortfolioTradeState tradeState) {
        this.tradeState = tradeState;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Portfolio [amount=" + amount + ", currency=" + currency + ", id=" + id + ", stockQuantity="
                + stockQuantity + ", stockTicker=" + stockTicker + ", tradeState=" + tradeState + "]";
    }

    

    
}
